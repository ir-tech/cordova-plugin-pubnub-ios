var exec = require('cordova/exec');

exports.main = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'main', arg0);
};

exports.pubNubHereNowForChannel = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'pubNubHereNowForChannel', arg0);
};

exports.publishMessage = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'publishMessage', arg0);
};

exports.removeChannelsMetaData = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'removeChannelsMetaData', arg0);
};

exports.removeAllPushNotification = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'removeAllPushNotification', arg0);
};

exports.enablePushNotification = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'enablePushNotification', arg0);
};

exports.pushNotificationOnChannel = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'pushNotificationOnChannel', arg0);
};

exports.messgaeCount = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'messgaeCount', arg0);
};

exports.deleteMessageFromChannel = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'deleteMessageFromChannel', arg0);
};

exports.historyOfLastThreeMessage = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'historyOfLastThreeMessage', arg0);
};

exports.historyOfChannel = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'historyOfChannel', arg0);
};

exports.listOfSubcribedUdid = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'listOfSubcribedUdid', arg0);
};

exports.listOfUdidFromChannel = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'listOfUdidFromChannel', arg0);
};

exports.signal = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'signal', arg0);
};

exports.sendMessageWithCompression = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'sendMessageWithCompression', arg0);
};

exports.addMessageAction = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'addMessageAction', arg0);
};

exports.pubNubWhereNow = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'pubNubWhereNow', arg0);
};

exports.pubNubGlobalHereNow = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'pubNubGlobalHereNow', arg0);
};

exports.pubNubHereNowForChannel = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'pubNubHereNowForChannel', arg0);
};

exports.pubNubSubscribeToPresence = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'pubNubSubscribeToPresence', arg0);
};

exports.pubNubUnsubFromPresence = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'pubNubUnsubFromPresence', arg0);
};

exports.unsubscribeFromChannels = function (arg0, success, error) {
    exec(success, error, 'pubNubios', 'unsubscribeFromChannels', arg0);
};
